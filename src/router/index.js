import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import TableScroll from '@/components/tableScroll'
import SelfUploadImg from '@/components/selfUploadImg'
import Activity20160311 from '@/components/activity/20160311'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
   {
      path: '/',
      name: 'Hello',
      component: Hello
   },
   {
      path: '/tableScroll',
      name: 'TableScroll',
      component: TableScroll
   },
   {
      path: '/selfUploadImg',
      name: 'SelfUploadImg',
      component: SelfUploadImg
   },
   {
      path: '/activity/20160311',
      name: 'Activity20160311',
      component: Activity20160311
   }
  ]
})