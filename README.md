# first-vue
学习vue框架
使用npm全局安装vue-cli(npm install -g vue-cli)
vue-cli vue脚手架具体详细步骤解说 https://www.jianshu.com/p/2769efeaa10a
在当前目录中启动网页服务(php):在目录中进入命令行窗口,输入php -S localhost:8080(就相当于给当前目录或工程配置域名)
> first vue project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test

# install sweetalert
npm install --save sweetalert 
import swal from 'sweetalert'
swal("hello world!")
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
