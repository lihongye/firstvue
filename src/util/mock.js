var Mock = require('mockjs');
exports.default = Mock.mock(/\/test\/mockData/, function(options){
	var template = {};
	if(options.url.indexOf("12345") != "-1"){
		template = {
		    "dataArray|40": [
		    {
		      "Tid|+1": 1,
		      "First":'@name()',
		      "sex|1": [
			    "男",
			    "女"
			  ],
			  "Age|20-50": 50,
			  "Score|1-100": 100
		    }
		  ]
		};
	}else if(options.url.indexOf("lihongye") != "-1"){
		template = {
		    'name': '@name()',
		    'age|1-100': 100,
		    'color': '@color'
		};
	}else if(options.url.indexOf("uplaodFile1") != "-1"){
		return options;
	}
	return Mock.mock(template);
});